FROM golang:1.12.7
MAINTAINER "Pontus Carlsson <pontus.carlsson@gmail.com>"

ARG KUSTOMIZE_VERSION=3.1.0

RUN apt-get update && \
    apt-get install -y git bash openssh-client curl

RUN curl -O -L https://github.com/kubernetes-sigs/kustomize/releases/download/v${KUSTOMIZE_VERSION}/kustomize_${KUSTOMIZE_VERSION}_linux_amd64 && \
    mv kustomize_${KUSTOMIZE_VERSION}_linux_amd64 kustomize && \
    chmod u+x kustomize

ENTRYPOINT ["kustomize"]
